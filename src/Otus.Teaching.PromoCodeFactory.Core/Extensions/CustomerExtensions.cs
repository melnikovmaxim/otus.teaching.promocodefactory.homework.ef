﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Extensions
{
    public static class CustomerExtensions
    {
        public static void AddPreferences(this Customer customer, IEnumerable<Preference> preferences)
        {
            customer.CustomerPreferences ??= new List<CustomerPreference>();

            foreach (var preference in preferences)
            {
                var customerPreference = new CustomerPreference()
                {
                    Customer = customer,
                    CustomerId = customer.Id,
                    Preference = preference,
                    PreferenceId = preference.Id
                };

                customer.CustomerPreferences.Add(customerPreference);
            }
        }
    }
}
