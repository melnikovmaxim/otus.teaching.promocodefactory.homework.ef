﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerRepository: IRepository<Customer>
    {
        Task<IEnumerable<Customer>> GetCustomersByPreferenceNameWith(string preferenceName);
        Task<Customer> GetCustomerWithPreferencesAndPromos(Guid id);
    }
}
