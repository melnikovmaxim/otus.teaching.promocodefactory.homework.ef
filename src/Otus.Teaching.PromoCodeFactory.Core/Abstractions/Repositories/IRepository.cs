﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<IEnumerable<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);
        
        Task AddAsync(T entity);
        
        Task UpdateAsync(T entity);

        Task RemoveAsync(Guid id);

        Task AddRange(IEnumerable<T> entities);

        Task<IEnumerable<T>> GetRangeAsync(IEnumerable<Guid> ids);

        Task<T> GetFirstAsync(Expression<Func<T, bool>> expression);
    }
}