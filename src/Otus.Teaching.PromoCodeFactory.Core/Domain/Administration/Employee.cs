﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Employee
        : BaseEntity
    {
        [MaxLength(30)]
        public string FirstName { get; set; } = null!;
        
        [MaxLength(30)]
        public string LastName { get; set; } = null!;

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(30)] 
        public string Email { get; set; } = null!;

        public Role Role { get; set; } = null!;

        [Range(0, int.MaxValue)]
        public int AppliedPromocodesCount { get; set; }
    }
}