﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.Administration
{
    public class Role
        : BaseEntity
    {
        [MaxLength(30)]
        public string Name { get; set; } = null!;
        
        [MaxLength(100)]

        public string Description { get; set; }
    }
}