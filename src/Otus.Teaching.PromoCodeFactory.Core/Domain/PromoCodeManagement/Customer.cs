﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer
        :BaseEntity
    {
        [MaxLength(30)]
        public string FirstName { get; set; } = null!;
        
        [MaxLength(30)]
        public string LastName { get; set; } = null!;

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(30)]
        public string Email { get; set; } = null!;

        public ICollection<CustomerPreference>? CustomerPreferences { get; set; }
        
        public ICollection<PromoCode>? PromoCodes { get; set; }
    }
}