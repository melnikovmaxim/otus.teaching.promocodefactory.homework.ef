﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Runtime;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class PromoCode
        : BaseEntity
    {
        [MaxLength(30)] 
        public string Code { get; set; } = null!;
        
        [MaxLength(100)]

        public string ServiceInfo { get; set; } = null!;

        public DateTime BeginDate { get; set; }

        public DateTime? EndDate { get; set; }
        
        [MaxLength(30)]

        public string PartnerName { get; set; } = null!;

        public Employee? PartnerManager { get; set; }

        public Preference Preference { get; set; } = null!;

        public Customer Customer { get; set; } = null!;
    }
}