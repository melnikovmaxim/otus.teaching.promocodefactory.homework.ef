﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T>: IRepository<T>
        where T: BaseEntity
    {
        protected readonly PromoDbContext _db;

        public EfRepository(PromoDbContext db)
        {
            _db = db;
        }
        
        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _db.Set<T>().AsNoTracking().ToListAsync();
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return _db.Set<T>().SingleAsync(e => e.Id == id);
        }

        public async Task AddAsync(T entity)
        {
            _ = await _db.Set<T>().AddAsync(entity);
            
            await _db.SaveChangesAsync();
        }

        public async Task AddRange(IEnumerable<T> entities)
        {
            _db.Set<T>().AddRange(entities);

            await _db.SaveChangesAsync();
        }

        public Task UpdateAsync(T entity)
        {
            _db.Set<T>().Update(entity);
            
            return _db.SaveChangesAsync();
        }

        public async Task RemoveAsync(Guid id)
        {
            var entity = await _db.Set<T>().SingleAsync(e => e.Id == id);

            _db.Set<T>().Remove(entity);

            await _db.SaveChangesAsync();
        }

        public async Task<IEnumerable<T>> GetRangeAsync(IEnumerable<Guid> ids)
        {
            return await _db.Set<T>().Where(e => ids.Contains(e.Id)).AsNoTracking().ToListAsync();
        }

        public async Task<T> GetFirstAsync(Expression<Func<T, bool>> expression)
        {
            var entity = await _db.Set<T>().FirstOrDefaultAsync(expression);

            if (entity is null)
            {
                throw new Exception($"Не получилось найти сущность с типом '{typeof(T).Name}' с выражением {expression}");
            }

            return entity;
        }
    }
}