﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : EfRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(PromoDbContext db) : base(db)
        {
        }

        public async Task<IEnumerable<Customer>> GetCustomersByPreferenceNameWith(string preferenceName)
        {
            var customers = await _db.Set<Customer>()
                .Include(c => c.CustomerPreferences)
                .ThenInclude(p => p.Preference)
                .Where(c => c.CustomerPreferences.Any(p => p.Preference.Name == preferenceName))
                .ToListAsync();

            return customers;
        }

        public async Task<Customer> GetCustomerWithPreferencesAndPromos(Guid id)
        {
            var customer = await _db.Set<Customer>()
                .Include(c => c.PromoCodes)
                .Include(c => c.CustomerPreferences)
                .ThenInclude(cp => cp.Preference)
                .SingleOrDefaultAsync(c => c.Id == id);

            if (customer is null)
            {
                throw new Exception($"Клиент с id '{id}' не найден");
            }

            return customer;
        }
    }
}
