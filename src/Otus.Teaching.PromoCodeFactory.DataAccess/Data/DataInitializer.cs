﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Sqlite.Storage.Internal;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public static class DataInitializer
    {
        public static async Task InitializeAsync(IServiceProvider serviceProvider)
        {
            var scope = serviceProvider.CreateScope();
            await using var dbContext = scope.ServiceProvider.GetService<PromoDbContext>();
            var dbCreator = dbContext.GetService<IDatabaseCreator>() as SqliteDatabaseCreator;
            var isExistsDb = await dbCreator!.ExistsAsync();

            if (isExistsDb)
            {
                return;
                //await dbCreator.EnsureDeletedAsync();
            }
            
            await dbCreator.EnsureCreatedAsync();

            await dbContext.Set<Employee>().AddRangeAsync(FakeDataFactory.Employees);
            await dbContext.Set<Customer>().AddRangeAsync(FakeDataFactory.Customers);
            await dbContext.SaveChangesAsync();

            var preferences = dbContext.Set<Preference>();
            var roles = dbContext.Set<Role>();

            await preferences.AddRangeAsync(FakeDataFactory.Preferences.Where(p => !preferences.Any(x => x.Id == p.Id)));
            await roles.AddRangeAsync(FakeDataFactory.Roles.Where(r => !roles.Any(x => x.Id == r.Id)));
            await dbContext.SaveChangesAsync();
        }
    }
}