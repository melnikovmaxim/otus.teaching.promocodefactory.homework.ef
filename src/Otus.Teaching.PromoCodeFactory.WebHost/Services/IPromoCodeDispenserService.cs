﻿using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public interface IPromoCodeDispenserService
    {
        Task GivePromoCodeToCustomerAsync(GivePromoCodeRequest request);
    }
}
