﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.Core.Extensions;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class CustomerDbService: ICustomerDbService
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IMapper _mapper;

        public CustomerDbService(
            IRepository<Customer> customerRepository,
            IRepository<Preference> preferenceRepository,
            IMapper mapper)
        {
            _customerRepository = customerRepository;
            _preferenceRepository = preferenceRepository;
            _mapper = mapper;
        }
        
        public async Task CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customer = _mapper.Map<Customer>(request);

            if (request.PreferenceIds?.Count > 0)
            {
                var preferences = await _preferenceRepository.GetRangeAsync(request.PreferenceIds);

                customer.AddPreferences(preferences);
            }

            await _customerRepository.AddAsync(customer);
        }

        public async Task EditCustomerAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = _mapper.Map<Customer>(request);

            customer.Id = id;

            if (request.PreferenceIds?.Count > 0)
            {
                var preferences = await _preferenceRepository.GetRangeAsync(request.PreferenceIds);

                customer.AddPreferences(preferences);
            }

            await _customerRepository.UpdateAsync(customer);
        }
    }
}