﻿using System;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public interface ICustomerDbService
    {
        Task CreateCustomerAsync(CreateOrEditCustomerRequest request);
        Task EditCustomerAsync(Guid id, CreateOrEditCustomerRequest request);
    }
}