﻿using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Services
{
    public class PromoCodeDispenserService : IPromoCodeDispenserService
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IRepository<PromoCode> _promoCodeRepository;

        public PromoCodeDispenserService(ICustomerRepository customerRepository, IRepository<PromoCode> promoCodeRepository)
        {
            _customerRepository = customerRepository;
            _promoCodeRepository = promoCodeRepository;
        }

        public async Task GivePromoCodeToCustomerAsync(GivePromoCodeRequest request)
        {
            var customers = await _customerRepository.GetCustomersByPreferenceNameWith(request.Preference);
            var promoCodes = new List<PromoCode>();

            foreach(var customer in customers)
            {
                promoCodes.Add(new PromoCode()
                {
                    Code = request.PromoCode,
                    BeginDate = DateTime.UtcNow,
                    ServiceInfo = request.ServiceInfo,
                    PartnerName = request.PartnerName,
                    Preference = customer.CustomerPreferences.First(x => x.Preference.Name == request.Preference).Preference,
                    Customer = customer
                });
            }

            await _promoCodeRepository.AddRange(promoCodes);
        }
    }
}
