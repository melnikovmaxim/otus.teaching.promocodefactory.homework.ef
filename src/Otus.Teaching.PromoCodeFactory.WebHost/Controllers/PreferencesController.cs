﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController: ControllerBase
    {
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IMapper _mapper;

        public PreferencesController(IRepository<Preference> preferenceRepository, IMapper mapper)
        {
            _preferenceRepository = preferenceRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить все предпочтения
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PreferenceShortResponse>>> GetAllPreferencesAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            var response = _mapper.Map<IEnumerable<PreferenceShortResponse>>(preferences);

            return Ok(response);
        }
    }
}
