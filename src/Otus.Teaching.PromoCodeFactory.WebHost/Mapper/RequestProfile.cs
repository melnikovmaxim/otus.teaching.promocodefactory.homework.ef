﻿using AutoMapper;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapper
{
    public class RequestProfile: Profile
    {
        public RequestProfile()
        {
            CreateMap<CreateOrEditCustomerRequest, Customer>();
            CreateMap<GivePromoCodeRequest, PromoCode>()
                .ForMember(d => d.Code, m => m.MapFrom(s => s.PromoCode))
                .ForMember(d => d.Preference, m => m.Ignore());
        }
    }
}