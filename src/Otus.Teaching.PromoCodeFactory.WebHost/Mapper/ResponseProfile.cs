﻿using AutoMapper;
using Microsoft.AspNetCore.Components;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Mapper
{
    public class ResponseProfile: Profile
    {
        public ResponseProfile()
        {
            CreateMap<Preference, PreferenceShortResponse>();
            CreateMap<PromoCode, PromoCodeShortResponse>();
            CreateMap<Role, RoleItemResponse>();
            CreateMap<Employee, EmployeeResponse>();
            CreateMap<Employee, EmployeeShortResponse>();
            CreateMap<Customer, CustomerResponse>()
                .ForMember(d => d.CustomerPreferences, mo => mo.Ignore())
                .AfterMap((s, d, context) =>
                {
                    var preferences = s.CustomerPreferences.Select(cp => cp.Preference).ToList();
                    d.CustomerPreferences = context.Mapper.Map<List<PreferenceShortResponse>>(preferences);
                });
            CreateMap<Customer, CustomerShortResponse>();
        }
    }
}